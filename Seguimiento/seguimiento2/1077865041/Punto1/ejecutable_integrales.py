import integrales as it
import numpy as np


a=0 #Limite inferior
b=np.pi #Limite superior
N=1000000 #Numero de puntos
N2=1000 #Numero de iteraciones para la grafica

if __name__=='__main__':
    #Apliquemos la clase integral
    a=it.integral(a,b,N,N2)
    #Se genera el valor de la integral
    print("La solución con Monte Carlo es aproximadamente", np.round(a.integral_monte(), 4))
    #Se genera el valor de la integral de forma analitica
    print("El valor de la integral es aproximadamente", a.integral_analitica())
    #Se grafican los valores de la integral en cada iteracion
    a.grafica()
