import numpy as np
import sympy as sp
from integracion import integracion

if __name__ == "__main__":

    xs = sp.Symbol('x')
    fs = xs **2 * sp.cos(xs)
    lim1 = 0
    lim2 = np.pi
    n = 10000

    resultado = integracion(xs, fs, lim1, lim2, n)
    print('el resultado exacto de la integral es:  {}'.format(resultado.int_sim()))
    print('el resultado de la integral por monte carlo es:  {}'.format(resultado.int_mc()))

    # llamamos el método para gráficar y comparar la solución exacta y por monte carlo
    resultado.graficar()