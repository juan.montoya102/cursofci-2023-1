import numpy as np
import matplotlib.pyplot as plt
from sympy import *

class integral_MC2():

    def __init__(self,n,a,b,x,y,f):

        self.n = n
        self.a = a
        self.b = b
        self.x = x
        self.y = y
        self.f = f

    def f_val(self):
        val = self.f(self.x)
        return  val

    def interior_p(self):
        p_i = (self.y <= self.f_val()) & (self.y > 0)
        return p_i
    
    def interior_n(self):
        p_n = (self.y >= self.f_val()) & (self.y < 0)
        return p_n


    def exterior_p(self):
        e_p = (self.y > self.f_val()) & (self.y > 0)
        return e_p
    
    def exterior_n(self):
        e_n = (self.y < self.f_val()) & (self.y < 0)
        return e_n
    
    def graficar(self):
        x_g = np.linspace(0,np.pi,1000)
        plt.plot(x_g,self.f(x_g))
        plt.plot(self.x[self.interior_p()],self.y[self.interior_p()],".",color = "orange",label = "Puntos interiores")
        plt.plot(self.x[self.interior_n()],self.y[self.interior_n()],".",color = "green")
        #plt.plot(self.x[self.exterior_p()],self.y[self.exterior_p()],".",color = "blue")
        #plt.plot(self.x[self.exterior_n()],self.y[self.exterior_n()],".",color = "blue",label="punto exteriores")
        plt.legend()
        plt.savefig("Funcion2.png")

    def Integral(self):
        A = 10* (self.b-self.a)/self.n*(self.interior_p().sum()-self.interior_n().sum())
        return A
    
class Integrales_SP():
    

    #inicializamos metodo constructor
    def __init__(self,g,x1,x2,x):

        self.g =g
        self.x1 = x1
        self.x2 = x2
        self.x = x
        
        
    def IntegralS(self):
        return integrate(self.g,(self.x,self.x1,self.x2))



    