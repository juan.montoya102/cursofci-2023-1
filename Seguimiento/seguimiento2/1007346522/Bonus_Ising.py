import numpy as np
import matplotlib.pyplot as plt

class Ising():

    global Kb 
    Kb = 1#.38e-23

    def __init__(self,Longitud,Temperatura,J,Sigma):

        #Dimensiones de la red
        self.L = int(Longitud)

        #Parametros
        self.J = J
        self.T = Temperatura
        self.Sig = Sigma

        if self.T <= 0:
            'Temperatura no es válida, valor por defecto T = 2'
            self.T = 2
        
        if self.Sig <= 0:
            'Sigma no es válido, valor por defecto Sigma = 2'
            self.Sig = 2
        
        self.B = 1/(Kb*self.T)

        self.N = self.L**2 # Dimensiones de la red

        # Llenando la Matriz
        self.Matriz = np.random.choice([-1,1],self.N).reshape(self.L,self.L)
        
        # Creando la Matriz auxiliar con los bordes en cero
        self.Matriz_ = np.zeros((self.L+2)*(self.L+2)).reshape(self.L+2,self.L+2)
        self.Matriz_[1:-1,1:-1] = self.Matriz

        # Creando la mascara para realizar la suma de los vecinos
        self.Mask = np.array([[0,1,0],[1,0,1],[0,1,0]])


    # Suma de los vecinos
    def h(self,i,j):

        # Submatriz 3x3 centrada en la posicion i,j
        M_ij = self.Matriz_[i:i+3,j:j+3]

        return sum(sum(M_ij*self.Mask))

    # Energia
    def H(self):
        E_ = 0
        for k in range(0,self.L):
            for l in range(0,self.L):
                E_ += -2*self.J*self.Matriz[k,l]*self.h(k,l)
        return E_

    # Probabilidad de cambio
    def Prob(self,Delta_E):
        return np.exp(-1*self.B*Delta_E)
    
    # Funcion para cambiar el estado de una posicion
    def Cambio(self,i,j):

        E_0 = self.H()
        self.Matriz[i,j] *= -1
        E_f = self.H()

        if E_f < E_0:
            return self.Matriz[i,j]
        
        if np.random.uniform(0,1) <= self.Prob((E_f-E_0)):
            return -1*self.Matriz[i,j]
        else:
            return self.Matriz[i,j]

    # Funcion para calcular la capacidad calorifica
    def Cv(self):
        E_prom= self.H()/self.N
        E2_prom = self.H()**2/self.N
        return self.B**2*(E2_prom - (E_prom)**2)/self.N

    # Funcion para realizar la simulacion
    def Simulacion(self,N_max = 5,Graf = False ):

        if not Graf: print('Inicial:\n',self.Matriz)

        for n in range(N_max):
            k,l = np.random.randint(0,self.L,2)
            self.Matriz[k,l] = self.Cambio(k,l)

        if not Graf: print('Final:\n',self.Matriz)

    # Graficando
    def Graf(self,T_min = 0.5,T_max= 7,N_max=5,N_points = 10):

        T_array = np.linspace(T_min,T_max,N_points)
        Cv_array = []

        for T_ in T_array:
            self.B = 1/(Kb*T_)
            self.Simulacion(N_max,Graf=True)
            Cv_array.append(self.Cv())

        print('Final:\n',self.Matriz)
        
        plt.plot(T_array,Cv_array,'o')
        plt.savefig('Plot_Cv.png')

        
