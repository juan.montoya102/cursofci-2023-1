from FunciondeOndaMC import funciondeOndaMC

if __name__ == "__main__":

    # Número de puntos para la integración con MC
    N = 1000
    # Masa de la partícula atrapada en el pozo de potencial
    electron_mass = 9.11e-31 # [kg]
    # tiempo en el cual se quiere evaluar la función de onda
    time = 50 # [s]

    # Se instancia la clase con estos parámetros
    funcion_de_onda_pozo = funciondeOndaMC(N, electron_mass, time)

    # Se hace uso del método de gráfica de los resultados
    funcion_de_onda_pozo.graph_wave_function_comparison()
