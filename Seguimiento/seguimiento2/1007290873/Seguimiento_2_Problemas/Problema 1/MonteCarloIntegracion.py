import numpy as np
import matplotlib.pyplot as plt
import sympy as sym
import scipy

class monteCarloIntegracion():
    
    """
    Clase que permite obtener el valor de la integral de la función (x^2) * cos(x),
    entre 0 y pi, con MonteCarlo (MC) y analíticamente usando sympy
    
    Permite graficar los puntos generados con MC que quedan bajo la curva de la función,
    y la convergencia del resultado en función del número N 

    Para la integración con MC, se hace una división del intervalo en dos,
    donde la función es positiva y donde es negativa
    """

    # Atributos de clase:

    # Rango en el que vamos a integrar

    initial_point = 0
    mid_point = np.pi/2
    final_point = np.pi

    # Constructor

    def __init__(self, max_steps):

        """
        Parámetros con lo que se instancia la clase

        max_steps: número de máximo de puntos con los cuales hacer la estimación de la integral con MC
        """

        # Definición de atributos de instancia

        self.N_array = np.linspace(1, max_steps, 100)

    # Definición de métodos

    # Función que pretendemos integrar

    def function(self, x):
        return (x**2) * np.cos(x)

    # Método para hallar el máximo valor de la función en el primer intervalo

    def first_max_value_function(self):

        # Hallamos el x correspondiente al máximo valor de la función en el primer intervalo
        # haciendo uso del método optimize.fmin de la librería scipy
        max_x = scipy.optimize.fmin(lambda x: -self.function(x), 0, disp = 0)
        # Retornamos el valor máximo que alcanza la función 
        return self.function(max_x[0])

    # Primer rango en el que vamos a integrar, donde la función es positiva: [0, pi/2]

    def random_square_first_interval(self, N):

        # Método que retorna el rectángulo de números aleatorios en el primer intervalo,
        # donde la función es positiva

        # Fijamos una semilla
        np.random.seed(2)

        # Definimos el máximo valor que alcanza la función a integrar en el primer intervalo
        max_f_value = self.first_max_value_function()

        # Generamos el rectángulo de número aleatorios
        x_random_array = np.random.uniform(self.initial_point, self.mid_point, size = N)
        y_random_array = np.random.uniform(0, max_f_value, size = N)

        return x_random_array, y_random_array

    # Segundo rango en el que vamos a integrar, donde la función es negativa: [pi/2, pi]

    def random_square_second_interval(self, N):

        # Método que retorna el rectángulo de números aleatorios en el segundo intervalo,
        # donde la función es negativa

        # Fijamos la misma semilla
        np.random.seed(2)
        
        # Definimos el mínimo valor que alcanza la función a integrar en el segundo intervalo
        min_f_value = self.function(np.pi)

        # Generamos el rectángulo de número aleatorios
        x_random_array = np.random.uniform(self.mid_point, self.final_point, size = N)
        y_random_array = np.random.uniform(min_f_value, 0, size = N)

        return x_random_array, y_random_array

    def bool_mask_first_area(self, N):

        # Método que retorna la máscara booleana correspondiente a los puntos que sí
        # están por debajo de la curva de la función en la primera región

        # Fijamos la misma semilla
        np.random.seed(2)

        # Se genera el cuadrado de puntos aleatorios en esta región
        x_random, y_random = self.random_square_first_interval(N)

        # Se evalúa la función en el arreglo aleatorio de x
        function_evaluated_random  = self.function(x_random)

        # Se crea la máscara booleana con la condición de puntos debajo de la curva
        inside_bool_mask = y_random <= function_evaluated_random

        return inside_bool_mask

    def bool_mask_second_area(self, N):

        # Método que retorna la máscara booleana correspondiente a los puntos que sí
        # están por encima de la curva de la función en la segunda región

        # Fijamos la misma semilla
        np.random.seed(2)

        # Se genera el cuadrado de puntos aleatorios en esta región
        x_random, y_random = self.random_square_second_interval(N)

        # Se evalúa la función en el arreglo aleatorio de x
        function_evaluated_random  = self.function(x_random)

        # Se crea la máscara booleana con la condición de puntos por encima de la curva
        inside_bool_mask = function_evaluated_random <= y_random

        return inside_bool_mask

    def area_first_interval(self, N):

        # Método para calcular el área del primer intervalo
        max_f_value = self.first_max_value_function()

        # Se obtiene el arreglo de la máscara booleana
        inside_bool_mask = self.bool_mask_first_area(N)

        # Se cuentan el número de puntos dentro de la curva
        points_inside = np.sum(inside_bool_mask)

        # Se obtiene la probabilidad, dividiendo por el número de puntos
        first_probability = points_inside / N

        # Se obtiene el área del cuadrado
        square_area = max_f_value * (np.pi / 2)

        # Se multiplica la probabilidad por el área del cuadrado, para obtener 
        # el área por debajo de la curva
        return first_probability * square_area

    def area_second_interval(self, N):

        # Método para calcular el área del segundo intervalo
        min_f_value = self.function(np.pi)

        # Se obtiene el arreglo de la máscara booleana
        inside_bool_mask = self.bool_mask_second_area(N)     

        # Se cuentan el número de puntos dentro de la curva
        points_inside = np.sum(inside_bool_mask)

        # Se obtiene la probabilidad, dividiendo por el número de puntos
        second_probability = points_inside / N

        # Se obtiene el área del cuadrado
        square_area = min_f_value * (np.pi / 2)

        # Se multiplica la probabilidad por el área del cuadrado, para obtener 
        # el área por encima de la curva
        return second_probability * square_area

    def area_MC_result(self, N):

        # Para hallar el área total, se suman las áreas de los dos puntos
        return self.area_first_interval(N) + self.area_second_interval(N)

    def convergence_for_MC(self):
        
        # Método para generar un arreglo con los valores de la integral para cada valor de N

        # Lista vacía donde ir guardando los resultados
        area_results = []

        # Se lleva a cabo el ciclo for para cada valor de N
        for N in self.N_array:
            
            # Se obtiene el valor del área correspondiente
            area_N = self.area_MC_result(int(N))
            area_results.append(area_N)
        
        return np.array(area_results)


    def area_analytical_result(self):
        
        # Método para hallar el valor analítico de la integral, usando sympy

        # Se define la variable simbólica
        x = sym.Symbol("x")

        # Se calcula la integral simbólica
        integrated_symbolic_f = sym.integrate((x**2) * sym.cos(x))
        # Se convierte en una función que es evaluable numericamente
        result_integrated_f = sym.lambdify(x, integrated_symbolic_f, "numpy")
        # Se obtiene el resultado de la integral, evaluando en los límites
        result_analytical = result_integrated_f(self.final_point) - result_integrated_f(self.initial_point)

        return result_analytical


    def graph_results(self):

        # Se muestran dos plots, en la gráfica: uno correspondiente a los puntos de generados con MC
        # y que quedan bajo la curva de la función; y otro de la convergencia del resultado como función
        # del número de pasos

        # Valores necesarios para hacer el primer plot,
        # puntos que quedan por dentro de la curva de la función

        x_random_1, y_random_1 = self.random_square_first_interval(int(self.N_array[-1]))

        inside_bool_mask_1 = self.bool_mask_first_area(int(self.N_array[-1]))

        x_random_2, y_random_2 = self.random_square_second_interval(int(self.N_array[-1]))

        inside_bool_mask_2 = self.bool_mask_second_area(int(self.N_array[-1]))

        N_string = str(int(self.N_array[-1]))

        # Array que se grafica en el segundo plot

        convergence_for_MC_array = self.convergence_for_MC()
        analytical_string = str(np.round(self.area_analytical_result(), 5))

        # Gráfica de los dos plots

        fig, ax = plt.subplots(2, 1, figsize = (10,16))
        ax[0].scatter(x_random_2[inside_bool_mask_2], y_random_2[inside_bool_mask_2], color = 'orange', label = "Puntos MC")
        ax[0].scatter(x_random_1[inside_bool_mask_1], y_random_1[inside_bool_mask_1], color = 'dodgerblue', label = "Puntos MC")
        ax[0].plot(np.linspace(0, np.pi, 200), self.function(np.linspace(0, np.pi, 200)), color = 'firebrick', lw = 3, label = r"f(x) = $x^2$cos($x$)")
        ax[0].legend()
        ax[0].set_title(r'Plot puntos generados con MC para calcular la integral, con N =' + N_string, fontsize = 16)
        ax[0].set_xlabel('$x$', fontsize = 16)
        ax[0].set_ylabel('f($x$)', fontsize=16)

        ax[1].plot(self.N_array, convergence_for_MC_array, color = 'mediumblue', lw = 3, label = 'MC')
        ax[1].hlines(y = self.area_analytical_result(), xmin = 0, xmax = self.N_array[-1], linewidth = 3, color = 'firebrick', label = 'Analítico:'+analytical_string)
        ax[1].set_title(r'Plot de la convergencia del resultado de la integral con MC', fontsize = 16)
        ax[1].legend(title = 'Resultado de la integral')
        ax[1].set_xlabel('N', fontsize = 16)
        ax[1].set_ylabel('Valor de la integral', fontsize=16)

        fig.suptitle(r'Gráfica de la integración con MC de la función $x^2$ cos($x$)', fontsize = 18)

        fig.tight_layout(pad = 5)
        plt.savefig("Integracion_con_MC.png")




    


