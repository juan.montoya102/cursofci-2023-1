import numpy as np
from scipy import optimize
import matplotlib.pyplot as plt
import sympy as sym

#Clase para resolver por Montecarlo la integral del problema 1
class int_Montecarlo():
    #Constructor
    def __init__(self):
        self.N=np.linspace(1,10000,100) #Atributo: Numero de iteraciones

    def funcion(self,x):
        return x**2*np.cos(x) #Función a integrar
    
    def maximo_funcion(self):
        maximo=optimize.fmin(lambda x:-self.funcion(x),0,disp=0) #Maximo de la funcion de 0 a pi
        return maximo[0] #Coordenada x del maximo de la función

    def cuadrado_1(self): #Integral del la función de 0 a pi/2, para ello se genera un cuadrado que vaya en x de 0 a pi/2 y en y de 0 a el maximo de la función.

        Arreglo_In_1_Area=[] #lista que guarda los resultados de la integral para el cuadrado 1
        Arreglo_x1=[] #lista que guarda el arreglo de numeros aleatorios en x para el ultimo N
        Arreglo_y1=[] #lista que guarda el arreglo de numeros aleatorios en y para el ultimo N
        Arreglo_Interior1=[]
        np.random.seed(0)

        for i in self.N: #se crea un for que corra en N y se guarda en listas los resultados paso a paso de la integral.
            i=int(i)
            if i==int(self.N[-1]): #condicional para guardar solo los datos que correspondan al ultimo N
                x1=np.random.uniform(0,np.pi/2,i) #Numeros aleatorios de 0 a pi/2 
                y1=np.random.uniform(0,self.funcion(self.maximo_funcion()),i) #Numeros aleatorios de  0 a el maximo de la función.
                Arreglo_x1.append(x1)
                Arreglo_y1.append(y1)

                interior1=y1<self.funcion(x1) #todos los puntos que esten por debajo de la función
                Arreglo_Interior1.append(interior1)
                In1=interior1.sum()/i #probabilidad
                Arreglo_In_1_Area.append(In1*(np.pi/2*self.funcion(self.maximo_funcion()))) #se multiplica la probabilidad por el area

            else: #condicional para guardar los datos de la integral para cada N
                x1=np.random.uniform(0,np.pi/2,i)
                y1=np.random.uniform(0,self.funcion(self.maximo_funcion()),i)

                interior1=y1<self.funcion(x1)
                In1=interior1.sum()/i
                Arreglo_In_1_Area.append(In1*(np.pi/2*self.funcion(self.maximo_funcion())))

        return Arreglo_x1[0],Arreglo_y1[0],Arreglo_Interior1[0],Arreglo_In_1_Area
    
    def cuadrado_2(self): #Integral del la función de pi/2 a pi, para ello se genera un cuadrado que vaya en x de pi/2 a pi y en y desde el minimo de la función(para este rango) a 0 
        #(se repiten todos los mismo pasos que en el cuadrado 1)
        Arreglo_In_2_Area=[]
        Arreglo_x2=[]
        Arreglo_y2=[]
        Arreglo_Interior2=[]
        np.random.seed(0)
        for i in self.N:
            i=int(i)
            if i==int(self.N[-1]):
                x2=np.random.uniform(np.pi/2,np.pi,i) #Numeros aleatorios de pi/2 a pi 
                y2=np.random.uniform(self.funcion(np.pi),0,i) #Numeros aleatorios  desde el minimo de la función(para este rango) a 0 
                Arreglo_x2.append(x2)
                Arreglo_y2.append(y2)
    
                interior2=y2>self.funcion(x2) #todos los puntos que esten por encima de la función
                Arreglo_Interior2.append(interior2)
                In2=interior2.sum()/i
                Arreglo_In_2_Area.append(In2*((np.pi/2)*self.funcion(np.pi)))
            else:
                x2=np.random.uniform(np.pi/2,np.pi,i)
                y2=np.random.uniform(self.funcion(np.pi),0,i)
    
                interior2=y2>self.funcion(x2)
                In2=interior2.sum()/i
                Arreglo_In_2_Area.append(In2*((np.pi/2)*self.funcion(np.pi)))
        return Arreglo_x2[0],Arreglo_y2[0],Arreglo_Interior2[0],Arreglo_In_2_Area
    
    def integral_funcionM(self): #Metodo que suma las integrales del cuadrado 1 y el cuadrado 2
        return np.array(self.cuadrado_1()[3])+np.array(self.cuadrado_2()[3])
    
    def exacto(self): #metodo para determinar la integral exacta de la funcion de 0 a pi por medio de sympy
        x=sym.Symbol('x')
        I=sym.integrate(x**2*sym.cos(x),(x,0,sym.pi))
        f=sym.lambdify(x,I,"numpy")
        return f(1)
    
    def grafica(self): #graficas de la integral de montecarlo

        plt.figure() #Grafica 1, muestra los puntos generados por montecarlo y la función a integrar dentro del rango definido.
        x=np.arange(0,np.pi,0.1)
        plt.plot(x,self.funcion(x),color="red",label="Función")
        plt.scatter(self.cuadrado_1()[0][self.cuadrado_1()[2]],self.cuadrado_1()[1][self.cuadrado_1()[2]],s=15,label="Montecralo Area 1")
        plt.scatter(self.cuadrado_2()[0][self.cuadrado_2()[2]],self.cuadrado_2()[1][self.cuadrado_2()[2]],color="green",s=15,label="Montecralo Area 1")
        plt.xlabel("x")
        plt.ylabel("f(x)")
        plt.title("integración por Montecarlo")
        plt.legend()
        plt.savefig("Integración por montecarlo")

        plt.figure() #grafica 2, compara la integral exacta obtenida por sympy y la integral obtenida por Montecarlo
        plt.plot(self.N,self.integral_funcionM(),label="Solución por Montecarlo")
        plt.axhline(self.exacto(),ls="--", color="orange",label="Solución exacta Sympy")
        plt.xlabel("Numero de iteraciones (N)")
        plt.ylabel("Valor de la integral")
        plt.title("Resultado de la integral por Montecarlo y exacta")
        plt.legend()
        plt.savefig("Resultado de la integral por montecarlo y exacta")