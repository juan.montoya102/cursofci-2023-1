from integracion_montecarlo import int_Montecarlo
import numpy as np

if __name__=="__main__":

    integracionM=int_Montecarlo()
    print("Integral del cuadrado 1=",integracionM.cuadrado_1()[3][-1])
    print("Integral del cuadrado 2=",integracionM.cuadrado_2()[3][-1])
    print("Resultado de la integral por Montecarlo=",integracionM.integral_funcionM()[-1])
    print("Resultado de la integral exacta",integracionM.exacto())
    grafica=integracionM.grafica()
