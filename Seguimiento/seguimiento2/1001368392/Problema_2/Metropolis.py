import numpy as np
class Metropolis():
    """
    Esta clase contiene un metodo para realizar integracion utilizando el metodo de Metropolis

    Requiere para ser definida:
    f: La funcion de la cual se busca el valor esperado
    w: Los pesos de los valores de f
    Nwalkers: # de caminantes
    Nsteps: # de pasos de cada caminante
    x0start: Limite inferior para la generacion de posiciones iniciales
    xfstart: Limite inferior para la generacion de posiciones iniciales
    """

    def __init__(self, f, w ,Nwalkers, Nsteps, x0start, xfstart):    
        self.f = f
        self.w = w 
        self.Nwalkers = Nwalkers
        self.Nsteps = Nsteps
        self.x0start = x0start
        self.xfstart = xfstart

    def Integrate(self):
        np.random.seed(42)
        # Genera posiciones iniciales
        X = np.random.uniform(self.x0start, self.xfstart, self.Nwalkers)
        # Inicializa los valores de la funcion muestreados
        N = np.array([])
        for i in range(self.Nsteps):
            # Genera pasos aleatoreos
            step = np.random.uniform(-0.1, 0.1, self.Nwalkers)
            # Calcula peso de la posiciones actuales y de las posiciones despues del paso
            Rtemp = self.w(X)
            Rprimetemp = self.w(X+step)
            # Compara las condiciones del metodo de Metropolis
            condmayor = np.abs(Rprimetemp/Rtemp) > 1 
            condmenor = (np.abs(Rprimetemp/Rtemp) <= 1) & (np.random.uniform(0,1, self.Nwalkers) < np.abs(Rprimetemp/Rtemp))
            condfinal = np.logical_or(condmayor, condmenor)
            # Dando los pasos que cumplen las condiciones
            X = X + step*condfinal
            # Excluyendo los primeros 4000 pasos para aumentar estabilidad
            if i >= 4000:
                N = np.append(N,self.f(X))
        return N.sum()/(self.Nwalkers*(self.Nsteps- 4000))

