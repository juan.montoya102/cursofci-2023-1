from HarmOsciMontecarlo import *
import matplotlib.pyplot as plt
"""
Nota 1: Este codigo tarda en ejercutarse. En mi caso toma alrededor de 5 minutos

Nota 2: La integracion de metropolis puede no converger para alpha de modulo grande. Los
hiperparametros de ajustaron hasta que los resultados en la region cerca a 1/2 convergieran
relativamente bien.
"""

if __name__ == "__main__":
    # Se da un arreglo de parametros alpha
    alpha = np.array([0.4, 0.45, 0.5, 0.55, 0.6])
    
    harm = HarmOscMontecarlo()
    
    # Se recorre los alpha dados para compararlos
    sols = np.array([])
    for i in alpha:
        sols = np.append(sols, harm.ground_state_metropolis(i))


    # Se grafican, y encontramos que la solucion de menor energia es alpha = 1/2, como se espera
    plt.figure(figsize=(12, 10))
    plt.title("Energia media de la funcion de onda en funcion del parametro variacional alpha")
    plt.scatter(alpha, sols)
    plt.xlabel("alpha")
    plt.ylabel("E")
    plt.grid()
    plt.savefig("ground_states.png")
    print("Graficas generadas exitosamente")