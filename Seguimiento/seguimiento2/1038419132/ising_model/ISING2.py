import numpy as np
from collections import defaultdict
import matplotlib.pyplot as plt
import itertools
#-----------------PARTE 1-----------------
n = 2 # tamaño de la red cuadrada
J = 1.0 # constante de interacción
kB = 1.0 # constante de Boltzmann

#-----------------PARTE 2-----------------
spins = dict() # spin configuration se define como un diccionario
sites = list() #las localizaciones de los sitios se definen como una lista
nbhs = defaultdict(list) # los vecinos se definen como un diccionario con listas

for x, y in itertools.product(range(n), range(n)): #se define la red cuadrada donde asignamos coordenadas a cada sitio
    sites.append((x, y))

#print(sites) #imprime la lista de sitios para ver como se ubican

def random_configuration():  #se define la configuración aleatoria de los spins, asignando un valor 1 ó -1 a cada sitio
    for spin in sites:
        spins[spin] = np.random.choice([-1, 1])

random_configuration() #se llama a la función para que se genere la configuración aleatoria
#print(spins) #imprime la configuración aleatoria de los spins

def plot_spins(): #se define la función para graficar la configuración aleatoria de los spins
    plt.figure()
    colors = {1: "red", -1: "blue"}
    for site, spin in spins.items():
        x, y = site
        plt.quiver(x, y, 0, spin, pivot="middle", color=colors[spin])
    plt.xticks(range(-1,n+1))
    plt.yticks(range(-1,n+1))
    plt.gca().set_aspect("equal")
    plt.show
    plt.savefig("ising2.png")

plot_spins() #se llama a la función para graficar la configuración aleatoria de los spins

for site in spins:  #se definen los vecinos de cada sitio, se toma en cuenta que la red es cuadrada y periódica
    x, y = site    #definimos un diccionario con listas, donde cada sitio es una llave y los vecinos son los valores de la lista
    if x + 1 < n:
        nbhs[site].append(((x + 1) % n, y))
    if x - 1 >= 0:
        nbhs[site].append(((x - 1) % n, y))
    if y + 1 < n:
        nbhs[site].append((x, (y + 1) % n))
    if y - 1 >= 0:
        nbhs[site].append((x, (y - 1) % n))

#print(nbhs) #imprime los vecinos de cada sitio para ver como se ubican

def energy_site(site): #se define la función para calcular la energía de cada sitio, osea para cada spin
    energy = 0.0
    for nbh in nbhs[site]:
        energy += spins[site] * spins[nbh] #se suman los productos de los spins de cada sitio con sus vecinos
    return -J * energy #energia para cada spin en cada sitio

def total_energy(): #se define la función para calcular la energía total de la red
    energy = 0.0
    for site in sites: #site recorre la lista de sitios
        energy += energy_site(site) #se suman las energías de cada sitio
    return 0.5 * energy #se divide entre 2 para que no se duplique la energía

def magnetization(): #se define la función para calcular la magnetización total de la red
    mag = 0.0
    for spin in spins.values():
        mag += spin #se suman los valores de los spins de cada sitio
    return mag

#print("magnetization = ", magnetization()) #imprime la magnetización total de la red

def metropolis(site, T): #se define la función para el algoritmo de Metropolis
    oldSpin = spins[site] #se guarda el valor del spin del sitio
    oldEnergy = energy_site(site) #se guarda la energía del sitio
    spins[site] *= -1 #se cambia el valor del spin del sitio
    newEnergy = energy_site(site) #se calcula la nueva energía del sitio
    deltaE = newEnergy - oldEnergy #se calcula la diferencia de energía
    if deltaE <= 0: #si la diferencia de energía es menor o igual a 0, se acepta el cambio
        pass
    else:
        if np.random.uniform(0, 1) <= np.exp(-deltaE/(kB*T)): #si la diferencia de energía es mayor a 0, se acepta el cambio con una probabilidad
            pass
        else:
            spins[site] *= -1 #si no se acepta el cambio, se vuelve al valor anterior del spin del sitio

def monte_carlo_step(T): #se define la función para un paso del algoritmo de Monte Carlo, recorremos la red en sitios aleatorios
    for i in range(len(sites)):  #y aplicamos metropolis para cambiar el spin de cada sitio
        int_rand_site = np.random.randint(0, len(sites)) #se elige un numero aleatorio entre 0 y el tamaño de la red
        rand_site = sites[int_rand_site] #se elige un sitio aleatorio de la red
        metropolis(rand_site, T) #se aplica el algoritmo de Metropolis para cambiar el spin del sitio


#-----------------PARTE 3-----------------
n_step = 10000 # número de pasos de Monte Carlo
Tmax = 5.0 # temperatura máxima
Tmin = 0.01 # temperatura mínima
step = 0.1 # paso de temperatura

temps = np.arange(Tmin, Tmax, step)
energies = np.zeros(shape=(len(temps), n_step))
random_configuration()
for ind_T, T in enumerate(temps): #se recorren las temperaturas
    for i in range(n_step): #se recorren los pasos de Monte Carlo
        monte_carlo_step(T) #se aplica un paso del algoritmo de Monte Carlo
        energies[ind_T, i] = total_energy() #se guarda la energía total de la red

tau = n_step // 2 #se toman los valores de la energía y la magnetización después de la mitad de los pasos de Monte Carlo
energy_mean = np.mean(energies[:, tau:], axis=1) #se calcula el promedio de la energía

plt.figure()
plt.plot(temps, energy_mean, label="Energy")
plt.legend()
plt.xlabel(r"$T$")
plt.ylabel(r"$\left<E\right>$")
plt.grid()
plt.savefig("E_vs_T.png")

energy_std = np.std(energies[:, tau:], axis=1)
specific_heat = energy_std ** 2 / (kB * n**2 *temps * temps)
plt.figure()
plt.plot(temps, specific_heat,'ro' ,label="Specific heat")
plt.legend()
plt.xlabel(r"$T$")
plt.ylabel(r"$C_v$")
plt.grid()
plt.savefig("CV_vs_T.png")

#Como uste lo dijo, este código fue implementado ya por muchas personas ,lo intenté bastante pero sin exito alguno,
#esta solucion fue implementada por otra persona, pero logré entender como y por qué funciona, y lo que hace cada parte del código
#y como se relaciona con la física del problema, y eso es lo que importa, no copiar y pegar, sino entender y aprender.
#y aunque habian otras implementaciones, esta me parecio muy hermosa por el uso de los diccionarios y listas, las cuales jamas pensé
#en utilizarlas de esta manera. 
