from integra import integra
import numpy as np

if __name__ == "__main__":

    def f(x): # function to integrate
        return x**2 * np.cos(x)

    n = 100000 # number of random points
    a = 0 # initial point
    b = np.pi/1 # final point
    I = integra(f,n,a,b) # create an instance of integra class
    print('Integral por montecarlo: ',-I.solve(),'Integral exacta: ',I.analytic()) # solve the equation