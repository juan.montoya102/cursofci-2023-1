
''' Archivo correspondiente a la ejecución de la clase creada para el 
problema de la partícula cargada en un campo magnético uniforme en z.'''

"""
NOTA 
4.5
el archivo Requeriments_part.txt no es correcto, tiene librerias que no son necesarias
"""
# Importo la clase del archivo fuente.
from Trayec_Particula import trayPart

# Inicialización de ejecución.
if __name__=='__main__':

    # Parámetros brindados para funcionamiento de clase.
    ek = 18.6           # Energía cinética partícula [eV].
    theta = 30          # Ángulo incidencia partícula respecto a B [grad].
    m = 0#9.109e-31       # Masa partícula; electrón [kg].
    q = -1.602e-19      # Carga partícula; electrón [C].
    b = 0#600e-6         # Densidad flujo magnético [T].
    ite = 10000         # Iteraciones para gráfica.
    n = 10       # Vueltas en gráfica (número de periodos).

    # Instanciar/llamar la clase.
    traypartb = trayPart(ek, theta, m, q, b, ite, n) 

    # Llamar métodos.
    traypartb.figPt3D()