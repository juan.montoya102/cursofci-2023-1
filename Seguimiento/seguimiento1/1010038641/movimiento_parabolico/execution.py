import numpy as np
from movimiento import movimiento
from movimiento import movimiento_modificado  


"""
NOTA
4.4
-No guarda el plot
- Control de errores, no funciona para ay=0

"""
if __name__ == "__main__":

    # valores iniciales 
    x0 = 0
    y0 = 0
    v0 = 20
    alfa = 45
    ay = 0#9.8
    
    # movimiento parabólico estándar, sin aceleración en x.
    ejemplo = movimiento(x0, y0, v0, alfa, ay)

    print('Movimiento parabólico estándar')
    print("la velocidad incial en x es {}".format(ejemplo.velocidad_inicial_x()), 'm/s')
    print("la velocidad incial en y es {}".format(ejemplo.velocidad_inicial_y()), 'm/s')
    print("el tiempo de vuelo es {}".format(ejemplo.tiempo_vuelo()), 's')
    print(ejemplo.graficar())

    # Movimiento con aceleración en x
    print('==============================')
    ax = 0
    ejemplo2 = movimiento_modificado(x0, y0, v0, alfa, ay, ax)
    #print('Movimiento parabólico con aceleración en x')
    #print("la velocidad incial es {}".format(ejemplo2.velocidad_inicial_x()), 'm/s')
    #print("la velocidad incial en y es {}".format(ejemplo2.velocidad_inicial_y()), 'm/s')
    #print("el tiempo de vuelo es {}".format(ejemplo2.tiempo_vuelo()), 's')

    print(ejemplo2.graficar())