import numpy as np
import matplotlib.pyplot as plt 
class movparabolico():
    def __init__(self, x0=0., y0=0., v0=0., alpha=0.):
        self.x0 = x0
        self.y0 = y0
        self.v0 = v0
        self.alpha = alpha
    
    # Velocidad inicial en x:
    def v0x(self):
        return self.v0*np.round(np.cos(self.alpha * np.pi/180),3)

    # Velocidad inicial en y:
    def v0y(self):
        return self.v0*np.round(np.sin(self.alpha * np.pi/180),3)

    # Tiempo de vuelo:
    def flightime(self,a):
        try:
            v0y = self.v0y()
            tmax = np.array([(- v0y + np.sqrt(v0y**2 - 2*a*self.y0))/(a), 
                            (- v0y - np.sqrt(v0y**2 - 2*a*self.y0))/(a)])
            tmax = np.max(tmax)
            return tmax
        except:
            return "Error en calculo de tmax, revisar parámetros" 
    
    # Posicion en x dado un tiempo:
    def x(self,t):
        v0x = self.v0x()
        return self.x0 + t*v0x
    
    # x final dado una aceeleracion en y:
    def xmax(self,a):
        tf = self.flightime(a)
        return self.x(tf)
    
    # Posicion en y dado un tiempo y una aceleracion en y:
    def y(self,t,a):
        v0y = self.v0y()
        return self.y0 + v0y*t + a* t**2 * 1/2

    # Altura maxima
    def ymax(self,a):
        tymax = - self.v0y()/a 
        return self.y(tymax)
    
    # Arreglo de tiempos del movimiento dada una aceleracion en y:
    def arrtime(self,a):
        t = np.linspace(0, self.flightime(a), 1000)
        return t 
    
    # Arreglo de posiciones en x dada una aceleracion en y:
    def arrx(self, a):
        t = self.arrtime(a)
        x = np.array([])
        for i in t:
            x = np.append(x, self.x(i))
        return x

    # Arreglo de posiciones en y dada una aceleracion en y:
    def arry(self, a):
        t = self.arrtime(a)
        y = np.array([])
        for i in t:
            y = np.append(y, self.y(i,a))
        return y

    # Metodo que grafica la trayectoria y la guarda en un archivo .png:
    def graph(self,a):
        
        tfin = self.flightime(a)
        x = self.arrx(a)
        y = self.arry(a)
        
        plt.figure(figsize=(10,8))
        plt.plot(x, y , label="Trayectoria",color="black")
        plt.scatter(self.x0, self.y0, label="Punto inicial",color="blue")
        plt.scatter(self.x(tfin), self.y(tfin,a), label="Punto final",color="red")
        plt.grid()
        plt.xlabel("Eje x")
        plt.ylabel("Eje y")
        plt.legend()
        plt.savefig("figura.png")