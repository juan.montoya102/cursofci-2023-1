from parabolic import tiroParabolico
from parabolic import parabolico2

if __name__=='__main__':

    # Confifuración.
    velinit = 100
    alpha = 42
    g = -9.8
    h0 = 10
    x0=0

    # Instanciar/llamar la clase.
    tirop = tiroParabolico(velinit, alpha, g, h0,x0) 

    # Llamar métodos.
    velx = tirop.velX() # Prueba unitaria, para no volver a la función sin necesidad.
    vely = tirop.velY()
    tmax = tirop.tMaxVuelo()
    arrTime= tirop.arrTime ()
    tirop.figMp()
    tiro=parabolico2(velinit, alpha, g, h0, x0,aceleracion=-8)
    tiro.grafica()
    

    print('Velocidad en x: {} m/s; velocidad en y: {} m/s.'.format(velx, vely))
    print('El tiempo máximo de vuelo es {} s.'.format(tmax))
    print('El tiempo máximo de vuelo es {} s.'.format(arrTime))

