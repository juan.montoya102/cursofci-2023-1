from particula import movParticula2

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
"""
NOTA:
4.6

- Revisar los limites de en los valores de las variables
"""

if __name__=="__main__":
    
    
    E = 18.6 * 1.602E-19 #Energía cinetica
    m = 9.1E-31 #masa del electron
    q = 1.602E-19 #carga del electron
    B = 0#6E-4 #B [T]
    ang = 30 #angulo [°]
    ciclos = 7 


    mov = movParticula2(1,E,m,q,B,ang,ciclos) 
    t = mov.viewCycles()
    mov.t = t   #python obliga a renombrar los metodos para utilizarlos como variables(?)
    mov.plot_traj() 

    print("**********INFO adicional***********")
    print("Radio de giro = {:.2E} m".format(mov.r))
    print("V_0 = {:.2E} m/s".format(mov.v))
    print("w = {:.2E} rad/s".format(mov.w))
    print("***********************************")
