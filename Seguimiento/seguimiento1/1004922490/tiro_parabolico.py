import math
import numpy as np
import matplotlib.pyplot as plt 


class tiroparabolico1():

    def __init__(self,velint,alpha,g,h0,x0):
        #print("inicializando clase tiro parabolico")

#parametros

        self.velint=velint
        self.radinalpha=math.radians(alpha)
        self.g=g
        self.h0=h0
        self.x0=x0

    def velX(self):
        vel_x=self.velint*round(math.cos(self.radinalpha),3)
        return vel_x

    def velY(self):
        vel_y=self.velint*round(math.sin(self.radinalpha),3)
        return vel_y

    def tMaxVuelo(self):
        try: 
         #tmax=-2*self.velY()/self:g
            tmax=(-self.velY()-np.sqrt(self.velY()**2-2*self.g*self.h0))/(self.g)
            return tmax
        except:
            return "error en calculo de tmax , revisar parametros"
    def arrTime(self):
        arr_time=np.arange(0.0,self.tMaxVuelo(),0.001)
        return arr_time

    def posX(self):
        posx=[self.x0+i*self.velX() for i in self.arrTime()]
        return posx
    def posY(self):
        posy=[self.h0+i*self.velY()+ (1/2)*self.g*i**2 for i in self.arrTime()]
        return posy

    def figMp(self):
         plt.figure(figsize=(10,8))

         plt.plot(self.posX(),self.posY())
         plt.savefig("tiro_parabolico.png")


class tiroparabolico2(tiroparabolico1):
    def __init__(self, velint, alpha, g, h0, x0, ax):
        self.ax=ax
        super().__init__(velint, alpha, g, h0, x0)

    def posaX(self):
        posax=[self.x0+i*self.velX()+ (1/2)*self.ax*i**2 for i in self.arrTime()]
        return posax
    def figMp(self):
         plt.figure(figsize=(10,8))

         plt.plot(self.posaX(),self.posY())
         plt.savefig("tiro_parabolico2.png")


    

    


        