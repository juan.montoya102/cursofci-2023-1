import numpy as np
import matplotlib.pyplot as plt

class MovimientoParabolico():
    
    # Inicializando Clase
    def __init__(self,x_0,y_0,v_0,angle,g= -9.8):
        # Se debería mostrar al user que la clase a inicializado
        print('Inicializando Movimiento Parabolico')  
        
        # Volviendo atrubutos los parámetros iniciales    
        self.x_0 = x_0
        self.y_0 = y_0
        if v_0 > 2.99e8: print('Digamos que es posible viajar más rápido que la luz...')
        self.v_0 = v_0
        self.angle = angle

        # Prefiero así para que el código se ejecute, ya me dirá si es debido o no
        if g == 0:
            print('Gravedad no válida, se toma por defecto -9.8')
            g = -9.8 
        self.g = g   

        # Descomponiendo la velocidad
        # Por errores numéricos, de una vez redondear las fun. trig.
        self.v_0x = self.v_0*round(np.cos(np.radians(self.angle)),3)
        self.v_0y = self.v_0*round(np.sin(np.radians(self.angle)),3)
    
    # Velocidad con respecto al tiempo
    def VelX(self,t):
        return self.v_0x # En este caso es constante
    
    def VelY(self,t):
        return self.v_0y + self.g*t 
    
    # Posicion con respecto al tiempo
    def PosX(self,t):
        # x(t) = x_0 + v_0x*t
        return self.x_0 + self.v_0x*t 
        
    def PosY(self,t):
        # y(t) = y_0 + v_0y*t + 0.5*g*t^2
        return self.y_0 + self.v_0y*t + 0.5*self.g*t**2
    
    # Tiempo de vuelo
    def t_max_X(self):
        return (-self.v_0y-((self.v_0y)**2-2*self.g*self.y_0)**0.5)/(self.g)
    
    # Tiempo para alcanzar la altura maxima
    def t_max_Y(self):
        return (-self.v_0y)/(self.g)
    
    # Alcance horizontal
    def X_max(self):
        return self.PosX(self.t_max_X())

    # Altura máxima
    def Y_max(self):
        return self.PosY(self.t_max_Y())
    
    # Datos de la trayectoria
    def info(self):
        CondIni = [self.x_0,self.y_0,self.v_0,self.angle,self.g]
        Carac = np.round([self.t_max_X(),self.X_max(),self.Y_max()],2) 
        Text1 = '----Condiciones iniciales:---- \nx_0 = {} \ny_0 = {} \nv_0 = {} \nAngulo = {} \nAceleración en y = {}'.format(*CondIni)
        Text2 = '\n----Características---- \nTiempo de vuelo = {} \nAlcance Horizontal = {} \nAltura máxima = {}'.format(*Carac)

        return Text1 +'\n'+ Text2

    def Graf(self,color= 'black',msg =''):

        t_array = np.linspace(0,self.t_max_X(),1000)

        R_f = np.round([self.X_max(),self.PosY(self.t_max_X())],2)
        R_y_max = np.round([self.PosX(self.t_max_Y()),self.Y_max()],2)

        plt.plot(self.PosX(t_array),self.PosY(t_array),c=color,label='Trayectoria {}'.format(msg))
        plt.scatter(self.PosX(t_array[0]),self.PosY(t_array[0]),label= 'Inicio = ({},{})'.format(self.x_0,self.y_0))
        plt.scatter(self.PosX(t_array[-1]),self.PosY(t_array[-1]),label='Final (X max)= ({},{})'.format(*R_f))
        plt.scatter(self.PosX(self.t_max_Y()),self.PosY(self.t_max_Y()), label='Y max = ({},{})'.format(*R_y_max))

        plt.xlabel('Posición X')
        plt.ylabel('Posición Y')
        plt.grid()
        plt.legend()
        plt.savefig('Plot_Movimiento.png')

#------------------------------------------------------------------------------------        
        
class MovimientoProyectil(MovimientoParabolico):

    def __init__(self, x_0, y_0, v_0, angle, g=-9.8,a_x = 0):
        print('Inicializando Movimiento Proyectil')
        super().__init__(x_0, y_0, v_0, angle, g)
        self.a_x = a_x

    def VelX(self, t):
        return self.v_0x + self.a_x*t

    def PosX(self, t):
        # x(t) = x_0 + v_0x*t + 0.5*a_x*t^2
        return self.x_0 + self.v_0x*t + 0.5*self.a_x*t**2
    
    def info(self):
        # Datos de la trayectoria, se modifica para agregar una condición inicial más,
        # propia del tipo de movimiento
        CondIni = [self.x_0,self.y_0,self.v_0,self.angle,self.g,self.a_x]
        Carac = np.round([self.t_max_X(),self.X_max(),self.Y_max()],2) 
        Text1 = '----Condiciones iniciales:---- \nx_0 = {} \ny_0 = {} \nv_0 = {} \nAngulo = {} \nAceleración en y = {} \nAceleración en x = {}'.format(*CondIni)
        Text2 = '\n----Características---- \nTiempo de vuelo = {} \nAlcance Horizontal = {} \nAltura máxima = {}'.format(*Carac)

        return Text1 +'\n'+ Text2   

#---------------------------------------------------------------------        