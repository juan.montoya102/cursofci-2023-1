from MovimientoEnCampo import *


"""
NOTA

5.0

MUY BUEN TRABAJO!!
"""

if __name__== '__main__':
    print(f'Your name is {__name__}')

    # Datos iniciales del problema
    R_inicial = [0.,0.,0.]
    Ek = 18.6 # eV
    Theta = 30 # Grados
    Phi = 10 # Grados
    Campo_B = 0#600 # T
    # m, q del electrón 

    # Inicializando
    Electron = MovimientoEnCampoMagnetico(R_inicial,Ek,Theta,Phi,Campo_B)
    Electron.Graf() # Generar gráfico
    print(Electron.info()) # Mostrar información en Terminal

