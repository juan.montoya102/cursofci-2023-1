# Desarrollo
### Santiago Galvis Duque
---

La fuerza producida por el Campo Magnético es de la forma
$$ \vec{F} =m\, \vec{a} =  q\, \vec{V} \times \vec{B} \\
\vec{a} = \frac{q}{m}\vec{v} \times \vec{B} = \frac{qB}{m}(\vec{v}_{||} + \vec{v}_{\perp}) \times \hat{k}\\
=\frac{qB}{m}(v_x\hat{i} +v_y\hat{j}) \times \hat{k} = \frac{qB}{m}(v_y\hat{i} -v_x\hat{j})\\
= \frac{qB \,v_\perp}{m}(\sin{\phi}\hat{i} -\cos{\phi}\hat{j} + 0 \hat{k}) $$ 

Veamos que la aceleración en dirección $\hat{k}$ es nula, por tanto $v_z = cte$. Además:
$$ \frac{d}{dt}(v²) = \frac{d}{dt}(\vec{v} \cdot \vec{v}) = 2\vec{a} \cdot \vec{v}\\ 

= 2qBm^{-1}(v_y\hat{i} -v_x\hat{j}) \cdot (v_x \hat{i}+v_y\hat{j} +v_z \hat{k}) \\
= 2\frac{qB}{m}(v_yv_x -v_xv_y) = 0 $$

Por tanto $v=cte$ e igualmente $v_\perp = cte$. Es decir, que la aceleración solo está cambiando la dirección de la velocidad, no su magnitud; así que se trata de un movimiento circular uniforme en el plano $xy$.

Entonces es correcto afirmar que
$$F = |q|B \, v_\perp = m\,a = m \frac{v_\perp²}{\rho} = cte$$
Siendo $\rho$ el radio que describe las cordenadas $x,y$. De forma que
$$\rho = \frac{m v_\perp}{|q|B}= cte$$
También se sabe que 
$$v_\perp = \omega \rho \Longleftrightarrow \omega = \frac{|q|B}{m}$$

En el plano $xy$ se cumple que
$$\rho² = (x(t)-h)²+(y(t)-k)²$$
Siendo $(h,k)$ la coordenada en donde se centra la circunferencia. 

Para encontrar este punto basta con darse cuenta que, en cualquier momento, el centro es la posición de la pertícula más un vector con magnitud $\rho$ y dirección la aceleración del movimiento, en especial cuando $t=0$:

$$(h,k) = (x_0,y_0)+ \rho \frac{\vec{a}}{a} =  (x_0,y_0)+ \rho \frac{\vec{a}}{a}\\
 = (x_0,y_0)+ \rho(\sin{\phi}, -\cos{\phi}) = (x_0+\rho\sin{\phi},y_0 -\rho\cos{\phi}) $$

 De manera que se puede escribir 

$$x(t) -h= x(t) - (x_0+\rho\sin{\phi}) = \rho \sin{(\omega t+\Omega)}\\
y(t) -k= y(t) - (y_0-\rho\cos{\phi}) = \rho \cos{(\omega t+\Omega)}$$

A partir de la condución inicial se encuentra que $\Omega = - \phi$

En conclusión, se escriben las ecuaciones de movimiento por componentes:

$$x(t) =x_0+\rho\sin{\phi}+ \rho \sin{(\omega t-\phi)}\\
y(t) -y_0-\rho\cos{\phi} + \rho \cos{(\omega t-\phi)}\\
z(t) = z_0 + v_{||}t$$
