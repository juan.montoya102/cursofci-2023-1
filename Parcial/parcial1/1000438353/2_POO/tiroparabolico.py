import math
import numpy as np
import matplotlib.pyplot as plt


class tiroParabolico():
    
#Esta clase grafica la trayectoria de un objeto bajo una aceleración en el eje x y y

    def __init__(self,velinit,alpha,g,h0,x0,ax):
        self.velinit=velinit
        self.radianalpha=math.radians(alpha)
        self.g=g
        self.h0=h0
        self.x0=x0
        self.ax=ax


    def velX(self):
        vel_x=self.velinit*round(math.cos(self.radianalpha),3)
        return vel_x

    def velY(self):
        vel_y=self.velinit*round(math.sin(self.radianalpha),3)
        return vel_y

    def tMaxVuelo(self):
        try:
            tmax=(self.velY() + np.sqrt(self.velY()**2+2*self.g*self.h0))/(self.g)
            #print("tiempo de caida",tmax)
            return tmax
        except:
            return "Error en cálculo de tmax, revisar parámetros"

    def alcanceMax(self):
      x_max=self.x0+self.velX()*self.tMaxVuelo()-1/2*self.ax*self.tMaxVuelo()**2
      #print(x_max)
      return x_max

    def alturaMax(self):
      h_max=self.h0 + self.velY()*self.tMaxVuelo()/2 - 1/2*self.g*(self.tMaxVuelo()/2)**2
      #print(h_max)
      return h_max
    
    def XenalturaMax(self):
      x_hmax=self.x0 + self.velX()*self.tMaxVuelo()/2 - 1/2*self.ax*(self.tMaxVuelo()/2)**2
      #print(h_max)
      return x_hmax

    def arrTime(self):
      aar_time=np.arange(0,self.tMaxVuelo(),0.001)
      return aar_time

    def posX(self):
      posx=[self.x0 + i*self.velX() - 1/2*self.ax*i**2 for i in self.arrTime()]
      return posx

    def posY(self):
      posy=[self.h0 + i*self.velY() - 1/2*self.g*i**2 for i in self.arrTime()]
      return posy

    def figMp(self):
      print(f"\nLa velocidad inicial en x es {np.round(self.velX(),2)} m/s.")
      print(f"La velocidad inicial en y es {np.round(self.velY(),2)} m/s.")
      print(f"El alcance máximo es {np.round(self.alcanceMax(),2)} m.")
      print(f"La altura máxima es {np.round(self.alturaMax(),2)} m.") 
      print(f"El tiempo de vuelo es {np.round(self.tMaxVuelo(),2)} s.")

      font = {'weight' : 'bold', 'size'   : 18}
      plt.rc('font', **font)
      plt.figure(figsize=(10,8))
      plt.title("Tiro parabólico con fricción")
      plt.plot(self.posX(),self.posY())
      plt.scatter(self.x0,self.h0,s=300,c="black",marker="*",label="Pos. inicial")
      plt.scatter(self.XenalturaMax(),self.alturaMax(),s=100,c="black",label=f"Altura máx. ({np.round(self.XenalturaMax(),2)}, {np.round(self.alturaMax(),2)})")
      plt.xlabel("$x$ [m]")
      plt.ylabel("$y$ [m]")
      plt.grid()
      plt.legend()
      plt.savefig("Tiro_parabolico.png")
      print(f"\nSe ha guardado la imagen como Tiro_parabolico.png\n\n")



class Objeto(tiroParabolico):

  #esta clase grafica la trayectoria que hace un objeto si se deja caer desde el punto máximo del tiro parabolico


  def __init__(self,velinit,alpha,g,h0,x0,ax):
    super().__init__(velinit,alpha,g,h0,x0,ax)
    
    #--------------Polimorfismo---------------------
    self.x0=self.XenalturaMax()                   # se cambian las condiciones iniciales del problema
    self.h0=self.alturaMax()                      # además como se deja caer el objeto, este tiene velocidad
    self.velinit=0                                # inicial igual a 0.

  def Lanzamiento(self):
     print("Si se deja caer un objeto desde el punto máximo descrito por el movimiento parabólico\neste sigue la trayectoria guardada en la imagen Trayectoria_objeto.png")
     font = {'weight' : 'bold', 'size'   : 18}
     plt.rc('font', **font)
     plt.figure(figsize=(10,8))
     plt.title("Objeto cayendo desde el punto máximo")
     #print(self.posX())
     plt.plot(super().posX(),super().posY(),"r--")
     plt.scatter(self.x0,self.h0,s=300,c="black",marker="*",label=f"Pos. inicial {np.round(self.x0,2)},{np.round(self.h0,2)}")
     plt.xlabel("$x$ [m]")
     plt.ylabel("$y$ [m]")
     plt.grid()
     plt.legend()
     plt.savefig("Trayectoria_objeto.png")
      


    