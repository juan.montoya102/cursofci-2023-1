import numpy as np
import matplotlib.pyplot as plt
import sympy

class Solucion_EDO:

    def __init__(self,fun,x0,y0,a,b):
        print('Inicializando clase')

        # Se espera que las condiciones inicales sean tales que no lleven a discontinuidades
        self.x0 = x0
        self.y0 = y0
        self.a = a
        self.b = b
        self.fun = fun

    # Definimos método de Euler
    def Euler(self,X,Y,h,i):
        Yi =  Y[i]+h*self.fun(X[i],Y[i])
        Xi = X[i] + h
        return Xi,Yi
    
    # Definimos método de Runge-Kutta 4
    def Rk4(self,X,Y,h,i):
        k1 = self.fun(X[i],Y[i])
        k2 = self.fun(X[i]+0.5*h,Y[i]+0.5*k1*h)
        k3 = self.fun(X[i]+0.5*h,Y[i]+0.5*k2*h)
        k4 = self.fun(X[i]+h,Y[i]+k3*h)
        Xi = X[i]+h
        Yi = Y[i]+h*(k1+2*k2+2*k3+k4)/6
        return Xi,Yi

    # Solución analítica utilizando la librería Sympy
    def Sympy_Sol(self,X,Y,h,Nmax,show= False):
        
        # Definiendo variables simbólicas
        xs = sympy.Symbol('x')
        ys = sympy.Function('y')
        fs = self.fun(xs,ys(xs))

        # Condiciones Iniciales
        condInit = {ys(X[0]):Y[0]}

        # Solución analítica
        sol_ed = sympy.dsolve(ys(xs).diff(xs) - fs,ics= condInit) # Por lo general aparecen subrayados "errores"
                                                                  # pero en el testeo nada salió mal
        
        # Si se quiere imprimir la solución
        if show: print(sol_ed)

        # Rellenando las listas de valores
        for i in range(0,Nmax):
            X.append(X[i]+h)
            Y.append(sol_ed.rhs.subs(xs,X[i+1]).evalf())
        return X,Y

    # Método solucionador de la ecuación diferencial en el intervalo establecido [a,b]
    def SolucionY(self,end,method = 'Euler',N=100,show = False):

        # Inicializando arreglos
        Y = [self.y0]
        X = [self.x0]

        # Definiendo paso
        if (end <= self.b) and (end != self.a):
            h = (end-self.a)/int(N) # El número de pasos debe ser entero
        else:
            print('Verifique el final del intervalo')
            raise ValueError
        
        # Verificando que el método no ingresado está dentro de la clase
        if not method in ['Euler','RK4','Sympy']:
            print('Método no encontrado')
            raise ValueError
    
        def Sol_method(X,Y,h,i): return None,None # Inicializando La variable que contendrá el método a utilizar


        # Dependiendo el método que el usuario prefiera (Por ahora solo está Euler, Runge-Kutta 4 y Analítico por Sympy)
        if method == 'Euler':
            Sol_method = self.Euler
            
        if method == 'RK4':
            Sol_method = self.Rk4

        if method == 'Sympy':
            X,Y = self.Sympy_Sol(X,Y,h,Nmax=N,show = show)
            return X,Y    
        
        # Rellenando listas (Solo para métdos numéricos)
        for i in range(0,N):
            Xi,Yi = Sol_method(X,Y,h,i)
            X.append(Xi)
            Y.append(Yi)
        return X,Y
        

    # Solución de la ecuación diferencial en un valor de x establecido
    def SolucionX(self,x,method = 'Euler'):
        X,Y = self.SolucionY(method = method, end=x)
        return f'y({X[-1]}) = {Y[-1]}'
    
    # Función Graficadora de la solución por el método definido
    def Grafica(self,method = 'Euler',name='Plot_Solucion_EDO'):
        X,Y = self.SolucionY(end=self.b,method = method)

        plt.figure()
        plt.plot(X,Y, label = 'Solución Numérica ({})'.format(method))

        plt.xlabel('x')
        plt.ylabel('y(x)')
        plt.grid()
        plt.legend()
        plt.title('Solucion EDO')
        plt.savefig(name+'.png')

    # Función de comparación entre dos métodos de solución
    def GraficaComparacion(self,method = 'Euler',method2 = 'Sympy' ,name = 'Plot_Soluciones_EDO'):

        if method == method2: print('No estás comparando nada')

        X_exact,Y_exact = self.SolucionY(end=self.b,method=method2) # Por defecto solución analítica
        X_numeric,Y_numeric = self.SolucionY(end=self.b,method = method)

        plt.figure()
        plt.plot(X_exact,Y_exact, label = 'Solución Exacta')
        plt.plot(X_numeric,Y_numeric, label = 'Solución Numérica ({})'.format(method))

        plt.xlabel('x')
        plt.ylabel('y(x)')
        plt.grid()
        plt.legend()
        plt.title('Comparación Soluciones EDO')
        plt.savefig(name+'.png')

# -------------------------------------------------------------------------------



