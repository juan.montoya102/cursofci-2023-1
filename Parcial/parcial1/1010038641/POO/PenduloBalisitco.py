import numpy as np
from matplotlib import pyplot as plt


class PenduloBalistico:
    # constructor
    def __init__(self, l, m, M, v0, g):
        print('Inicializando clase péndulo balístico')
        self.l = l
        self.m = m
        self.M = M
        self.v0 = v0
        self.g = g
        self.t = np.arange(0, 10, 0.01)

    # si la velocidad de la bala es muy alta comparada con la masa del bloque, la solución diverge
    # y es como si el bloque diera infinitas vueltas. La relación entre la velocidad de la bala,
    # la masa de la bala y del bloque que se econtró es la siguiente:
    def angulo(self):
        if self.v0 > (2 * np.sqrt(self.g * self.l) * (self.m + self.M)) / self.m:
            raise ValueError('Revisar parametros de velocidad y masa del bloque')
        else:
            return np.round(
                np.rad2deg(np.arccos(1 - (self.m * self.v0 / (self.m + self.M)) ** 2 * (1 / (2 * self.g * self.l)))), 2)

    # velocidad incial del conjunto bloque masa
    def vel_c(self):
        return np.round(np.sqrt(2 * self.g * self.l), 2)


# ==========================================================================================================

'''Clase que aplica polimorfismo para calcular la velocidad mínima para que el péndulo de una vuelta'''

'''el método libre consistirá en calcular la velocidad inicial que debe tener el conjunto bala-bloque
dado un ángulo particular entre 90° y 180°'''


class PooPendulo(PenduloBalistico):
    def __init__(self, l, m, M, v0, g, theta):
        print('Inicializando clase que aplica polimorfismo')
        super().__init__(l, m, M, v0, g)
        self.theta = np.deg2rad(theta)

    # definimos la función para calcular la velocidad mínima del conjunto bala-bloque para que de vueltas
    def v_min(self):
        v_min = np.round(np.sqrt(5 * self.g * self.l), 2)
        return v_min

    # ahora, la función para calcular la velocidad inicial de la bala
    def vmin_b(self):
        vminb = np.round(2 * (self.m + self.M) * self.v_min() / self.m, 2)
        return vminb

    # método libre, velocidad que debe tener el conjunto bloque - bala para un ángulo especificado
    def v_angulo(self):
        return np.round(np.sqrt(self.g * self.l * (2 - 3 * np.cos(self.theta))), 2)

    # la solución es una ecuación armónica, que dadas las condciones iniciales es un seno
    def fun(self, t):
        g = 9.8
        l = 0.5
        w = np.sqrt(g/l)
        return np.sin(t)

    # graficamos las oscilaciones
    def graficar(self):
        plt.title('Oscilación péndulo balístico')
        plt.plot(self.t, self.fun(self.t))
        plt.xlabel('$t$')
        plt.ylabel('$θ(t)$')
        plt.show()