from pendulobalistico import Pendulo_balistico
from pendulobalistico import pendulo_balistico2
import numpy as np
if __name__=="__main__":
    #Los valores a ingresar deben ser numeros, no string. La gravedad, las masas, la longitude de la cuerda y el angulo deben ser positivos. 
    a=45 #angulo de desviación
    mb=0.2 #masa de la bala
    Mp=1.5 ##Masa del bloque en el pendulo
    g=9.8 #gravedad
    R=0#0.5 #Longitud del pendulo

   
    pb=Pendulo_balistico(mb,Mp,g,R,a)
    print("Velocidad inicial de la bala: {} m/s para un angulo de desviación: {} ° ".format(np.round(pb.velocidadbala(),3),a))
    graf=pb.graf()
    
    #Herencia y polimorfismo
    v0=10 #velocidad inicial
    pb2=pendulo_balistico2(mb,Mp,g,R,v0)
    print("Velocidad minima para una vuelta: {} m/s".format(pb2.velocidadminbala()))
    print("Angulo de desviación: {} ° para una velocidad: {} m/s".format(pb2.desviacionangulo(),v0))