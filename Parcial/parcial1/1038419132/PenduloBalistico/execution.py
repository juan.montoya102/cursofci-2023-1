from PenduloBalistico import Pendulus
from PenduloBalistico import Pendulus2
import matplotlib.pyplot as plt


if __name__=="__main__":

    m = 1 #masa de la bala
    M = 2 #masa del bloque
    ang = 10 #ángulo de desviacion del sistema masa-bloque
    g = 9.81 #gravedad
    R = 2 #radio de la cuerda
    t = 3 #tiempo de oscilacion del sistema masa-bloque despues de alcanzar su angulo maximo
    p1 = Pendulus2(ang,m,M,R,g,t) #creo un objeto de la clase Pendulus2
    
    plt.plot(p1.movoscilatorio()[0],p1.movoscilatorio()[1]) #grafico el movimiento oscilatorio del sistema masa bloque
    plt.ylim(-R,R)
    plt.xlim(-R,R)
    plt.xlabel("x [m]")
    plt.ylabel("y [m]")
    plt.savefig("PenduloBalistico.png")
    plt.show()

    print()
