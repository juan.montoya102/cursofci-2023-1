from sympy import Function, dsolve, Eq, Derivative, sin, cos, symbols, exp
from SolODE import *
from penduloSimple import *
if __name__ == "__main__":
    
    # Parametros para la solucion de ODE
    yfun = Function('y')
    ysym = symbols('y')
    x = symbols('x')
    dyfun = exp(-x)
    dysym = exp(-x)
    x0 = 0
    xf = 1
    y0 = -1
    n = 100000

    sist1 = SolODE(dyfun, dysym, x0, y0, xf, n)
    print(f"La solucion para la ecuacion diferencias ordinaria dy/dx = {dyfun} es {sist1.exactsol()}")
    # Graficas para Euler y para ambas soluciones
    sist1.grapheuler()
    sist1.comparesols()


    # Parametros para la solucion del pendulo simple 
    th0 = 1
    dth0 = 0
    gl = 16
    ti = 0
    tf = 5
    n = 50
 
    pend1 = penduloSimple(th0, dth0, gl, ti, tf, n)
    # Solucion analitica
    print(f"La solucion analitica para el pendulo simple con las condiciones iniciales dadas es {pend1.solExacta()}")
    # Grafica que compara ambas soluciones
    pend1.comparesols()
    # Notese que Euler falla dado que n es muy bajo. Para mas iteraciones el resultado mejora
