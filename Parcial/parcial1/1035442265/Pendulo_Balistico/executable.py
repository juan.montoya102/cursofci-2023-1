
from Pendulo_Balistico import PenduloBalistico
from Pendulo_Balistico import PenduloBaslistico2
import math

if __name__=="__main__":

    m = 0.5       #Masa de la bala en Kg
    M = 1         #Masa del bloque en Kg
    l =0# 1        #longitud del pendulo en metros
    h= 0.001     #Elevación del pendulo en metros
    g = 9.8

    P_b = PenduloBalistico(m,M,l,h,g)

    P2 = PenduloBaslistico2(m,M,l,h,g)
    print("Angulo del impacto:{}".format(P_b.DesviacionAngulo()))
    #print("Velocidad de la bala:{}".format(P_b.VelocidadBala()))
    #print("Velocidad minima:{}".format(P2.Velocidadmin()))

    #print("array theta:{}".format(P_b.ArrayTheta()))

    #print("array theta:{}".format(P_b.PosX()))
    #print("array theta:{}".format(P_b.PosY()))

    P_b.figMp()
    print("Angulo en grados:{}".format(P2.DesviacionAngulo()))
