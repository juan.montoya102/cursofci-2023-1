import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

class PenduloSimple:
  def __init__(self,a,b,n,v_0,fun,theta_0,fun2,ecuacion):

    self.a=a
    self.b=b
    self.n=n
    self.v_0=v_0
    self.fun=fun
    self.fun2=fun2
    self.theta_0=theta_0
    self.ecuacion=ecuacion
    

    
  def H(self):
    h= (self.b-self.a)/self.n
    return h

  def TIEMPO(self):
    t=np.arange(self.a,self.b,self.H())
    return t
  
  def V(self):
    theta=[self.theta_0]
    v=[self.v_0]
    k=self.TIEMPO()
    for i in range (self.n-1):
      v.append(v[i]+self.H()*self.fun(k[i],theta[i]))
      theta.append(theta[i]+self.H()*self.fun2(k[i],v[i+1]))
    return v, theta    

  #solucion exacta
  """
  def ecuacion(self):
    s=[self.fun,self.fun2]
    return s"""
  def condi(self):
    o=[self.v_0,self.theta_0]
    return o
    


  def solr(self):
    sol= odeint(self.ecuacion, self.condi(), self.TIEMPO())
    return sol

    
    # Gráfica de la solución para theta
    
  def figMp2(self):
         plt.figure(figsize=(10,8))
         plt.plot(self.TIEMPO(),self.V()[0], color='pink', label='solucion numerica')
         plt.plot(self.TIEMPO(),self.solr()[:,0], color='gold',label='solucion real',lw=5)
         plt.title('Solucion Pendulo')
         plt.legend()
         plt.savefig("grficab.png")






