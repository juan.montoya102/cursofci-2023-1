#metodo de euler
import numpy as np
import matplotlib.pyplot as plt 
from scipy.integrate import odeint

class metodoE():
  def __init__(self,a,b,n,y_0,fun):
    print("inicializando clase")

    self.a=a
    self.b=b
    self.n=n
    self.y_0=y_0
    self.fun=fun

    
  def H(self):
    h= (self.b-self.a)/self.n
    return h

  def X(self):
    x=np.arange(self.a,self.b,self.H())
    return x
  
  def Y(self):
    
    y=[self.y_0]
    k=self.X()
    for i in range (self.n-1):
      y.append(y[i]+self.H()*self.fun(k[i],y[i]))
    return y  
  


    
  #solucion exacta
  def solr(self):
    sol= odeint(self.fun,self.y_0,np.linspace(self.a,self.b,self.n))
    return sol

  def figMp(self):
         plt.figure(figsize=(10,8))
         plt.plot(self.X(),self.Y(), color='pink', label='solucion numerica')
         plt.plot(self.X(),self.solr(), color='gold',label='solucion real')
         plt.title('Solucion EDO')
         plt.legend()
         plt.savefig("EDO1.png")

  


