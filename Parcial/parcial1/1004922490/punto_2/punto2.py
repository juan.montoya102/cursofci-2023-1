
import numpy as np


class pendulob():
  def __init__(self,v_0,m,M,R,g):
    
    self.v_0=v_0
    self.m=m
    self.M=M
    self.R=R
    self.g=g

  def V(self):
    v = (self.m*self.v_0)/(self.m+self.M)
    return v

  def H(self):
    h = ((self.V())**2)/2*self.g
    return h
    
  def THETA(self):
    theta = round(np.arccos((self.R-self.H())/self.R),1)
    return theta

  def GRADO(self):
    grado = (self.THETA()*180)/np.pi
    return grado



class pendulobb(pendulob):

  def __init__(self,m,M,R,g):
    super().__init__(self,m,M,R,g)
  

  def VC(self):
    vc = np.sqrt(self.R*self.g)
    return vc

  def VB(self):
    vb = np.sqrt((self.VC()**2)+4*self.g*self.R)
    return vb

  def U(self):
    u = ((self.m+self.M)*self.VB())/self.m
    return u 

 







