import random
import matplotlib.pyplot as plt

class Dado:
    def __init__(self, caras):
        self.caras = caras
    
    def lanzar(self):
        return random.randint(1, self.caras)

class SistemaDados:
    def __init__(self, n):
        self.n = n
        self.dados = [Dado(6) for _ in range(n)]
    
    def tirar_consecutivamente(self, lanzamientos):
        resultados = []
        for _ in range(lanzamientos):
            suma = sum([dado.lanzar() for dado in self.dados])
            resultados.append(suma)
        return resultados
    
    def tirar_simultaneamente(self, lanzamientos):
        resultados = []
        for _ in range(lanzamientos):
            suma = sum([random.randint(1, dado.caras) for dado in self.dados])
            resultados.append(suma)
        return resultados


