import numpy as np
import matplotlib.pyplot as plt


n = 1000000 # NCantidad de experimentos


# N=2

# Se crea lista que contiene los lanzaminetos obtenidos
dado1 = []
dado2 = []

# Generación de número aleatorio por lanzamiento
for i in range(0,n):
    dado1.append(np.random.randint(1,7))
    dado2.append(np.random.randint(1,7))
conteo1 = []

# Contador de la suma de los lanzamientos
for i in range(0,len(dado1)):
    conteo1.append(dado1[i] + dado2[i])

arreglo_conteo1 = np.asarray(conteo1)
max_cont1 = np.max(arreglo_conteo1)

# Histograma para los conteos
plt.hist(conteo1,density=False, bins=11)
plt.axvline(arreglo_conteo1.mean(), color='k', linestyle='dashed', linewidth=1,label= 'media')
plt.title("Histograma para N = 2")
plt.xlabel("Suma total en lanzamiento")
plt.ylabel("Ocurrencia")
plt.legend()
plt.show()

# N=3

# Se crea lista que contiene los lanzaminetos obtenidos
dado1 = []
dado2 = []
dado3 = []

# Generación de número aleatorio por lanzamiento
for i in range(0,n):
    dado1.append(np.random.randint(1,7))
    dado2.append(np.random.randint(1,7))
    dado3.append(np.random.randint(1,7))
conteo2 = []

# Contador de la suma de los lanzamientos
for i in range(0,len(dado1)):
    conteo2.append(dado1[i] + dado2[i] + dado3[i])

arreglo_conteo2 = np.asarray(conteo2)
max_cont2 = np.max(arreglo_conteo2)

# Histograma para los conteos
plt.hist(conteo2,density=False, bins=16)
plt.axvline(arreglo_conteo2.mean(), color='k', linestyle='dashed', linewidth=1,label= 'media')
plt.title("Histograma para N = 3")
plt.xlabel("Suma total en lanzamiento")
plt.ylabel("Ocurrencia")
plt.legend()

plt.show()

# N=4

# Se crea lista que contiene los lanzaminetos obtenidos
dado1 = []
dado2 = []
dado3 = []
dado4 = []

# Generación de número aleatorio por lanzamiento
for i in range(0,n):
    dado1.append(np.random.randint(1,7))
    dado2.append(np.random.randint(1,7))
    dado3.append(np.random.randint(1,7))
    dado4.append(np.random.randint(1,7))
conteo3 = []

# Contador de la suma de los lanzamientos
for i in range(0,len(dado1)):
    conteo3.append(dado1[i] + dado2[i] + dado3[i] + dado4[i])

arreglo_conteo3 = np.asarray(conteo3)
max_cont3 = np.max(arreglo_conteo3)

# Histograma para los conteos
plt.hist(conteo3,density=False, bins=21)
plt.axvline(arreglo_conteo3.mean(), color='k', linestyle='dashed', linewidth=1,label= 'media')
plt.title("Histograma para N = 4")
plt.xlabel("Suma total en lanzamiento")
plt.ylabel("Ocurrencia")
plt.legend()
plt.show()

#  Por la hipotesis erdoguica se puede decir que no hay diferencia entre lanzar los dados simultaneamente o uno por uno
#  ya que la probabilidad de que salga un numero es la misma en ambos casos, por lo tanto la suma de los lanzamientos.